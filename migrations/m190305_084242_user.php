<?php

use yii\db\Migration;

/**
 * Class m190305_084242_user
 */
class m190305_084242_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'fullName' => $this->string(),
            'status' => $this->smallInteger(),
            'admin'=> $this->smallInteger(),
            'email' => $this->string()->unique(),
            'phone' => $this->string()->unique(),
            'login' => $this->string()->unique(),
            'passwordHash' => $this->string(),
            'authKey' => $this->string(),
            'city' => $this->string(),
            'street' => $this->string(),
            'home' => $this->string(),
            'flat' => $this->string(),
        ], $tableOptions);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%user}}');
    }
}
