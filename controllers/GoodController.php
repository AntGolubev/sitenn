<?php

namespace app\controllers;

use app\models\Good;
use yii\data\ArrayDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;

class GoodController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex() {

        $dataProvider = new ArrayDataProvider(
            [
                'allModels' => Good::findAll(),
                'modelClass' => Good::class,
            ]
        );

        return $this->render('index', [
            'dataProvider' => $dataProvider
        ]);

    }

}