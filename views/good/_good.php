<?php
/**
 * @var $model \app\models\Good
 */
use yii\helpers\Html;
?>

<div class="good">
    <h2><?= Html::encode($model->title) ?></h2>
    <div class="good__description">
        <?= Html::encode($model->description) ?>
    </div>
    <div class="good__price">
        <?= $model->price ?>
    </div>
</div>
