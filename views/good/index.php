<?php
/**
 * @var $this \yii\web\View
 * @var $dataProvider \yii\data\ArrayDataProvider
 */
use yii\widgets\ListView;
?>
<div class="good-list">
    <?php try {
        echo ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => '_good',
            'summary' => false
        ]);
    } catch (\Exception $e) {
        echo 'Выброшено исключение: ',  $e->getMessage(), "\n";
    }
    ?>
</div>
