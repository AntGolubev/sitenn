<?php

/* @var $this yii\web\View */

$this->title = 'Тестовое задание!';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Тестовое задание!</h1>

        <p class="lead">Над внешним видом не заморачивался, только функциональность.</p>

        <p><a href="https://bitbucket.org/toAshGrey/sitenn/src">Исходный код приложения</a></p>

        <p>
            <?= \yii\helpers\Html::a('Перейти на страницу регистрации', ['/user/register'], ['class' => 'btn btn-lg btn-success']) ?>
        </p>
    </div>

</div>
