<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
$addresses = \yii\helpers\Json::encode(array_values(\app\models\User::getAllAddress()));

$this->registerJsFile('//api-maps.yandex.ru/2.1/?apikey=400c2870-ddee-42cb-89ec-4e689ed0c72f&load=package.standard&lang=ru-RU');
$this->registerJs(
    "
     var mapContainer = $('#yandexMap');
    var addresses = {$addresses};
    var yandexMap;

    if (mapContainer.length) {
        ymaps.ready(init);
    }
    
    function init() {
    
        yandexMap = new ymaps.Map(\"yandexMap\", {
            center: [56.31558, 43.922487],
            zoom: 10,
            controls: ['zoomControl', 'fullscreenControl']
        });
        
        yandexMap.behaviors.disable('scrollZoom');
        
        var counterArray = 0;
        addresses.forEach(function(address, i, arr) {
            ymaps.geocode(address).then(function (res) {
                yandexMap.geoObjects.add(res.geoObjects.get(0));
                counterArray++;
                if (counterArray === arr.length) {
                    yandexMap.setBounds(yandexMap.geoObjects.getBounds(), {checkZoomRange: true}).then(function(){ if(yandexMap.getZoom() > 10) yandexMap.setZoom(15);});
                }
            });
        });
    }
    
    ", \yii\web\View::POS_END
);
?>
<div class="user-index">

    <div id="yandexMap"></div>

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            'id',
            'fullName',
            'status',
            //'admin',
            'email:email',
            'phone',
            'login',
            //'passwordHash',
            //'city',
            //'street',
            //'home',
            //'flat',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
