<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = 'Профиль';
$addresses = \yii\helpers\Json::encode([$model->address]);

$this->registerJsFile('//api-maps.yandex.ru/2.1/?apikey=400c2870-ddee-42cb-89ec-4e689ed0c72f&load=package.standard&lang=ru-RU');
$this->registerJs(
    "
    var mapContainer = $('#yandexMap');
    var addresses = {$addresses};
    var yandexMap;

    if (mapContainer.length) {
        ymaps.ready(init);
    }
    
    function init() {
    
        yandexMap = new ymaps.Map(\"yandexMap\", {
            center: [56.31558, 43.922487],
            zoom: 10,
            controls: ['zoomControl', 'fullscreenControl']
        });
        
        yandexMap.behaviors.disable('scrollZoom');
        
        var counterArray = 0;
        addresses.forEach(function(address, i, arr) {
            ymaps.geocode(address).then(function (res) {
                yandexMap.geoObjects.add(res.geoObjects.get(0));
                counterArray++;
                if (counterArray === arr.length) {
                    yandexMap.setBounds(yandexMap.geoObjects.getBounds(), {checkZoomRange: true}).then(function(){ if(yandexMap.getZoom() > 10) yandexMap.setZoom(15);});
                }
            });
        });
    }
    
    ", \yii\web\View::POS_END
);

?>
<div class="user-update">

    <div id="yandexMap"></div>

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
