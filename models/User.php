<?php

namespace app\models;

use Yii;
use yii\base\NotSupportedException;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $fullName
 * @property int $status
 * @property int $admin
 * @property string $email
 * @property string $phone
 * @property string $login
 * @property string $passwordHash
 * @property string $city
 * @property string $street
 * @property string $home
 * @property string $flat
 * @property string $password
 * @property string $captcha
 * @property-read bool $isAdmin
 * @property-read bool $isFizLico
 * @property-read string $address
 */
class User extends \yii\db\ActiveRecord  implements \yii\web\IdentityInterface
{
    // По уму надо бы вытащить в отдельную форму, для большей гибкости, но так как приложение тестовое, для регистрации мы просто закроем из массового присваивания небезопасные свойства, в нужных случаях сеттер будет вызываться вручную, плюс установим сценарии для регистрации
    const SCENARIO_REGISTER = 'register';

    public $password;
    public $captcha;

    const STATUS_URLICO = 10;
    const STATUS_FIZLICO = 20;


    // Тут можно дальше расширять сколько угодно и выборка из БД, и зависимые друг от друга значения и тд

    private static $cities = [
        'Москва',
        'Санкт-Петербург',
        'Нижний Новгород',
    ];

    /**
     * @return array
     */
    public static function getCities(){
        return self::$cities;
    }

    private static $streets = [
        'Ленина',
        'Пушкина',
        'Лермонтова'
    ];

    /**
     * @param null $city_id
     * @return array
     */
    public static function getStreets($city_id= null){
        return self::$streets;
    }

    /**
     * Назначаем именна константам STATUS
     * @return array
     */
    public static function getStatuses()
    {
        return [
            static::STATUS_URLICO => 'Юридическое лицо',
            static::STATUS_FIZLICO => 'Физическое лицо',
        ];
    }

    /**
     * Получаем имя спрятанное за константой STATUS
     *
     * @param  string $default значение по умолчанию, если ничего не найдется
     * @return string|null
     */
    public function getStatusLabel($default = null)
    {
        $statuses = self::getStatuses();
        return isset($statuses[$this->status]) ? $statuses[$this->status] : $default;
    }

    /**
     * @return array
     */
    public static function getAllAddress(){
        return ArrayHelper::map(self::find()->all(), 'id', 'address');
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            'requiresValues' => [['fullName', 'status', 'email', 'phone', 'city', 'street'], 'required'],

            'statusValues' => ['status', 'in', 'range' => array_flip(self::getStatuses())],

            'emailIs' => ['email', 'email'],
            'emailUnique' => [['email'], 'unique'],

            'phoneValues' => ['phone', 'match', 'pattern' => '/^\d+$/', 'message' => 'Только цифры'],
            'phoneUnique' => ['phone', 'unique'],

            'loginValues' => ['login', 'string', 'max' => 255],
            'loginUnique' => ['login', 'unique'],

            'cityValues' => ['city', 'string'],

            'streetValues' => ['street', 'string'],

            'homeValues' => ['home', 'string', 'max' => 4],

            'flatValues' => ['flat', 'string', 'max' => 4],

            'passwordValues' => ['password', 'string'],

            'passwordRequire' => ['password', 'required', 'on' => self::SCENARIO_REGISTER],

            'captcha' => ['captcha', 'captcha', 'on' => self::SCENARIO_REGISTER],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fullName' => 'ФИО',
            'status' => 'Статус',
            'admin' => 'Админ',
            'email' => 'Email',
            'phone' => 'Телефон',
            'login' => 'Логин',
            'passwordHash' => 'Password',
            'password' => 'Пароль',
            'city' => 'Город',
            'street' => 'Улица',
            'home' => 'Дом',
            'flat' => 'Кв.',
            'captcha' => 'Капча',
        ];
    }

    /**
     * @param bool $insert
     * @return bool
     * @throws \yii\base\Exception
     */
    public function beforeSave($insert)
    {
        if ($this->password) {
            $this->setPassword($this->password);
        }
        return parent::beforeSave($insert);
    }

    /**
     * @return bool
     */
    public function register() {
        if ($this->save()) {

            $this->sendEmail();
            return Yii::$app->user->login($this);
        }
        return false;
    }

    public function sendEmail() {
        try {
            return \Yii::$app->mailer->compose('userRegister', ['user' => $this])
                ->setFrom([\Yii::$app->params['noReplyEmail'] => 'Автоинформатор'])
                ->setTo(\Yii::$app->params['adminEmail'])
                ->setSubject('Новый пользователь')
                ->send();
        } catch (\Exception $exception) {
            return false;
        }
    }

    /**
     * @return bool
     */
    public function getIsAdmin() {
        return $this->admin ? true : false;
    }

    /**
     * @return bool
     */
    public function getIsFizLico() {
        return $this->status == self::STATUS_FIZLICO;
    }

    /**
     * @throws \yii\db\StaleObjectException
     */
    public function adminRightSet(){
        if (!$this->isAdmin){
            $this->updateInternal(['admin' => 1]);
        }
    }

    /**
     * @throws \yii\db\StaleObjectException
     */
    public function adminRightUnset() {
        if ($this->isAdmin){
            $this->updateInternal(['admin' => null]);
        }
    }

    public function getAddress() {
        return "{$this->city} {$this->street} {$this->home} {$this->flat}";
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return self::findOne(['id' => $id]);
    }

    /**
     * @param mixed $token
     * @param null $type
     * @return void|\yii\web\IdentityInterface
     * @throws NotSupportedException
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by login
     *
     * @param string $username
     * @return static|null
     */
    public static function findByLogin($login)
    {
        return static::findOne(['login' => $login]);
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->passwordHash);
    }

    /**
     * @param $password
     * @throws \yii\base\Exception
     */
    public function setPassword($password)
    {
        $this->passwordHash = Yii::$app->security->generatePasswordHash($password);
    }
}
