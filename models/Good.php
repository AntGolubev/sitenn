<?php
/**
 * Created by PhpStorm.
 * User: mail
 * Date: 05.03.2019
 * Time: 12:12
 */

namespace app\models;

use yii\base\BaseObject;

/**
 * Good model
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property-read float $price
 * @property float $priceUr
 * @property float $priceFiz
 */
class Good extends BaseObject
{
    public $id;
    public $title;
    public $description;
    public $priceUr;
    public $priceFiz;

    // Для скорости сделано как mock-объект, но можно сделать хранение в БД, да и вообще где угодно, но это уже вопрос реализации
    private static $goods = [
        1 => [
            'id' => 1,
            'title' => 'Тестовый товар 1',
            'description' => 'Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной "рыбой" для текстов на латинице с начала XVI века.',
            'priceUr' => 1200,
            'priceFiz' => 800,
        ],
        2 => [
            'id' => 2,
            'title' => 'Тестовый товар 2',
            'description' => 'В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн.',
            'priceUr' => 1200,
            'priceFiz' => 800,
        ],
        3 => [
            'id' => 3,
            'title' => 'Тестовый товар 3',
            'description' => 'Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.',
            'priceUr' => 1200,
            'priceFiz' => 800,
        ],
    ];

    /**
     * @return Good[]|null
     */
    public static function findAll(){
        $return = null;
        foreach (self::$goods as $good) {
            $return[] = new static(self::$goods[$good['id']]);
        }
        return $return;
    }

    /**
     * @param $id
     * @return Good|null
     */
    public static function findById($id) {
        return isset(self::$goods[$id]) ? new static(self::$goods[$id]) : null;
    }

    /**
     * @return float
     */
    public function getPrice() {
        return \Yii::$app->user->identity->isFizLico ? $this->priceFiz : $this->priceUr;
    }
}